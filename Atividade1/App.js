import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Quadrado,GerarQuadrados} from './src/Quadrado';

export default function App() {
  return (
    <View style={styles.container}>
      <Quadrado lado={'5VW'} cor='#7CF8D3' ></Quadrado>
      <Quadrado lado={'8VW'} cor='#F17F3A' ></Quadrado>
      <Quadrado lado={'11VW'} cor='#56D1F6' ></Quadrado>
      <Quadrado lado={'14VW'} cor='#DF49C1' ></Quadrado>
      <Quadrado lado={'17VW'} cor='#8648ED' ></Quadrado>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: '#000000',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
  },
});
