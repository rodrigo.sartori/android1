import React, { useState } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

export default props =>{
    const [valor,setValor] = useState(props.valor)

    function fazDobroValor(){
        setValor( valor * 2)
    }

    return(
        <React.Fragment>

                <View style = {styles.valorInicial}>
                    <Text style = {styles.texto}>Valor: {props.valor}</Text>
                </View>

                <View style = {styles.resultado}>
                    <Button title = "calcular dobro" onPress = {fazDobroValor} ></Button>
                    <Text style = {styles.texto}> funcionou {valor}</Text>
                </View>

                        
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    valorInicial : {
        flex: 2,
        width : '100%',
        backgroundColor: '#f0f8ff',
        alignItems: 'center',
        justifyContent: 'center',
        },
    resultado : {
        flex : 3, 
        width : '100%',
        flexDirection : 'row',
        backgroundColor: '#00ffff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    texto :{
        fontVariant : ['small-caps'],
        color : "#dc143c",
    }
})