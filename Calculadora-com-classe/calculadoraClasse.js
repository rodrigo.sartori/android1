import React, { Component } from 'react'
import { StyleSheet, Text, TextInput, View, Button, Alert } from 'react-native';


export default class Operacoes extends React.Component {
    state = {
        valor1: '',
        valor2: '',
        resultado: ''
    }

    alterarValor1(val1) {
        this.setState({ valor1: val1 })
    }
    alterarValor2(val2) {
        console.log(val2)
        this.setState({ valor2: val2 })
    }
    alterarResultado(res) {
        this.setState({ resultado: res })
    }

    Somar(valor1, valor2) {
        this.alterarResultado(parseFloat(valor1) + parseFloat(valor2))
    }
    Subtrair(valor1, valor2) {
        this.alterarResultado(parseFloat(valor1) - parseFloat(valor2))
    }
    Multiplicar(valor1, valor2) {
        this.alterarResultado(parseFloat(valor1) * parseFloat(valor2))
    }
    Dividir(valor1, valor2) {
        if (parseInt(valor2) != 0) {
            this.alterarResultado(parseInt(valor1) / parseInt(valor2))
        } else {
            this.alterarResultado('não deve fazer divisão por 0')
        }

    }


    render() {
        return (
            <>
                <Text style={{ alignSelf: "center", justifyContent: 'flex-start' }}> Calculadora</Text>

                <View style={styles.containerNumeros}>
                    <TextInput placeholder="Digite o 1° número: " value={this.state.valor1} style={{ backgroundColor: "#6495ed" }} onChangeText={val1 => this.alterarValor1(val1)}></TextInput>
                    <TextInput placeholder="Digite o 2° número: " value={this.state.valor2} style={{ backgroundColor: "#6495ed" }} onChangeText={val2 => this.alterarValor2(val2)}></TextInput>
                </View>

                <View style={styles.containerBotoes}>
                    <Button title="Somar" onPress={() => this.Somar(this.state.valor1, this.state.valor2)}></Button>
                    <Button title="Subtrair" onPress={() => this.Subtrair(this.state.valor1, this.state.valor2)}></Button>
                    <Button title="Multiplicar" onPress={() => this.Multiplicar(this.state.valor1, this.state.valor2)}></Button>
                    <Button title="Dividir" onPress={() => this.Dividir(this.state.valor1, this.state.valor2)}></Button>
                </View>

                <View style={styles.containerBotoes}>
                    <Text>Resultado : {this.state.resultado}</Text>
                </View>

            </>
        )
    }
}

const styles = StyleSheet.create({

    containerBotoes: {
        flex: 9,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    containerNumeros: {
        flex: 5,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-around'
    },
});
