import React, { useState } from 'react';
import { StyleSheet, Button, View, Text, TextInput, Alert } from 'react-native';

export default props => {
    const [valor1, setValor1] = useState('')
    const [valor2, setValor2] = useState('')
    const [resultado, setresultado] = useState('')
    return (<React.Fragment>


        <Text style={{ alignSelf: "center", justifyContent: 'flex-start' }}> Calculadora</Text>

        <View style={styles.containerNumeros}>
            <TextInput placeholder="Digite o 1° número: " value={valor1} style={{ backgroundColor: "#6495ed" }} onChangeText={valor1 => setValor1(valor1)}></TextInput>
            <TextInput placeholder="Digite o 2° número: " value={valor2} style={{ backgroundColor: "#6495ed" }} onChangeText={valor2 => setValor2(valor2)}></TextInput>
        </View>

        <View style={styles.containerBotoes}>
            <Button title="Somar" onPress={() => Somar(valor1, valor2)}></Button>
            <Button title="Subtrair" onPress={() => Subtrair(valor1, valor2)}></Button>
            <Button title="Multiplicar" onPress={() => Multiplicar(valor1, valor2)}></Button>
            <Button title="Dividir" onPress={() => Dividir(valor1, valor2)}></Button>
        </View>

        <View style={styles.containerBotoes}>
            <Text>Resultado : {resultado}</Text>
        </View>

    </React.Fragment>)

    function Somar(valor1, valor2) {
        setresultado(parseFloat(valor1) + parseFloat(valor2))
    }
    function Subtrair(valor1, valor2) {
        setresultado(parseFloat(valor1) - parseFloat(valor2))
    }
    function Multiplicar(valor1, valor2) {
        setresultado(parseFloat(valor1) * parseFloat(valor2))
    }
    function Dividir(valor1, valor2) {
        if (parseInt(valor2) != 0) {
            setresultado(parseInt(valor1) / parseInt(valor2))
        } else {
            setresultado('não deve fazer divisão por 0')
        }

    }
}

const styles = StyleSheet.create({

    containerBotoes: {
        flex: 9,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    containerNumeros: {
        flex: 5,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-around'
    },
});
